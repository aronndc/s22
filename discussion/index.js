// console.log("Hello World!");

// ARRAY METHODS

/*
	Mutator Methods
		- seeks to modify the contents of an array.
		- are function that mutate an array after they are created. These methods manipulate original array performing various tasks such as adding or removing elements.
*/

let fruits = ["Apple", "Orange", "Kiwi", "Watermelon"];

/*
	push()
		- adds an element in the end of an array and returns the array's length.

		Syntax:
			arrayName.push(element)
*/

console.log("Current Fruits Array:");
console.log(fruits);

// Adding element/s

fruits.push("Mango");
console.log(fruits);
let fruitsLength = fruits.push("Melon");
console.log(fruitsLength);
console.log(fruits);

fruits.push("Avocado", "Guava");
console.log(fruits);

/*
	pop()
		- removed the last element in our array and returns the removed element (when applied inside a variable)

	Syntax:
		arrayName.pop()
*/

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated array from the POP method:")
console.log(fruits);


/*
	unshift()
		- adds one or more elements at the beginning of an array
		- returns the length of the array (when presented inside a variable)

	Syntax:
		arrayName.unshift(elementA);
		arrayname.unshift(elementA, elementB);
*/

fruits.unshift("Lime", "Banana");
console.log("Mutated array from the UNSHIFT method:")
console.log(fruits);

/*
	shift()
		- removes an element at the beginning of our array and return the removed element

	Syntax:
		arrayName.shift()
*/

let removedFruit2 = fruits.shift();
console.log(removedFruit2);
console.log("Mutated array from the SHIFT method:")
console.log(fruits);

/*
	splice()
		- allows to simultaneously remove element from a specified index number and adds an element

	Syntax:
		arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
*/

let fruitsSplice = fruits.splice(1, 2, "Cherry", "Lychee");
console.log("Mutaed arrays from splice method:")
console.log(fruits);


// Using splice without adding elements
let removedSplice = fruits.splice(3, 2);
console.log(fruits);
console.log(removedSplice);

/*
	sort()
		 - rearranges the array element in alphanumeric order

	Syntax:
	arrayName.sort()
*/

fruits.sort();
console.log("Mutated array from SORT method:")
console.log(fruits);

let mixedArr = [12, "May", 36, 94, "August", 5, 6.3, "September", 10, 100, 1000];
console.log(mixedArr);
console.log(mixedArr.sort());

/*
	reverse()
		- reverses the order or the element in an array

	Syntax:
		arrayName.reverse();
*/

fruits.reverse()
console.log("Mutated array from REVERSE method:");
console.log(fruits);

// For sorting the items in descending order.

fruits.sort().reverse();
console.log(fruits);

	/*MINI ACTIVITY:
	 - Debug the function which will allow us to list fruits in the fruits array.
	 	-- this function should be able to receive a string.
	 	-- determine if the input fruit name already exist in the fruits array.
	 		*** If it does, show an alert message: "Fruit already listed on our inventory".
	 		*** If not, add the new fruit into the fruits array ans show an alert message: "Fruit is not listed in our inventory."
	 	-- invoke and register a new fruit in the fruit array.
	 	-- log the updated fruits array in the console


	 	function registerFruit () {
	 		let doesFruitExist = fruits.includes(fruitName);

	 		if(doesFruitExst) {
	 			alerat(fruitName "is already on our inventory")
	 		} else {
	 			fruits.push(fruitName);
	 			break;
	 			alert("Fruit is now listed in our inventory")
	 		}
	 	}
	 	
	*/

/*	 	function registerFruit(fruitName) {
	 	    let doesFruitExist = fruits.includes(fruitName);
	 	    if (doesFruitExist) {
	 	        alert(fruitName + " is already on our inventory!");
	 	    } else {
	 	        fruits.push(fruitName);
	 	     // break
	 	        alert(fruitName + " is now listed in our inventory!");
	 	    }
	 	}

registerFruit("Dragon Fruit");
registerFruit("Banana");
console.log(fruits);*/



console.log(" ");
console.log(" ");
console.log(" ");

let carBrands = ["Mitsubishi", "Honda", "Toyota", "Isuzu", "Mazda"];
console.log(carBrands);

carBrands.push("Chevrolet");
console.log(carBrands);

carBrands.unshift("Opel");
console.log(carBrands);

let removedBrand = carBrands.pop();
console.log(carBrands);
console.log("Removed brand is: " + removedBrand);

carBrands.shift();
console.log(carBrands);

carBrands.sort();
console.log(carBrands);


/*
	Non-Mutator Methods
		- These are functions or methods than do not modify of change an array after they are created.
		- These methods do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays and printing the output.
*/
console.log(" ");
let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DN"];
console.log(countries);

/*
	indexOf()
		- returns the index number of the first matching element found in an array. If no match as found, the result will be -1. The search process will be done from our first element proceeding to the last element.

	Syntax:
		arrayName.indexOf(searchValue);
		arrayName.indexOf(searchValue, fromIndex)
*/

let firstIndex = countries.indexOf("PH");
console.log("Result of indexOf method: " + firstIndex);
firstIndex = countries.indexOf("PH", 4);
console.log("Result of indexOf method: " + firstIndex);
firstIndex = countries.indexOf("PH", 7);
console.log("Result of indexOf method: " + firstIndex);
firstIndex = countries.indexOf("PH", -1);
console.log("Result of indexOf method: " + firstIndex);

console.log(" ");

/*
	lastIndexOf()
		- returns the index number of the last matching element found in an array. The search process will be done from the last element proceeding to the first.

	Syntax:
		arrayName.lastIndexOf(searchValue, fromIndex)
*/


let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf method: " + lastIndex);
lastIndex = countries.lastIndexOf("PH", 4);
console.log("Result of lastIndexOf method: " + lastIndex);

/*
	slice()
		- portions / slices elements from our array adn returns a new array

	Syntax:
		arrayName.slice(startingIndex)
		arrayName.slice(startingIndex, endingIndex		
*/

console.log(" ");

console.log(countries);

let slicedArrA = countries.slice(3);
console.log("Result from slice method:")
console.log(slicedArrA);
console.log(countries);

console.log(" ");

let slicedArrB = countries.slice(2, 5)
console.log("Result from slice method:")
console.log(slicedArrB);
console.log(countries);

console.log(" ");

let slicedArrC = countries.slice(-3);
console.log("Result from slice method:")
console.log(slicedArrC);
console.log(countries);

/*
	toString()
		- returns as an array as a string separated by commas
		- is used internally by JS when an object / array needs to be displayed as text(like in HTML), or when an object / array needs to be used as a string.

	Syntax:
		arrayName.toString();
*/

console.log(" ");

let stringArray = countries.toString();
console.log("Result from toString method:")
console.log(stringArray);

/*
	concat();
		- combines two or more arrays and returns the combined result

	Syntax:
		arrayA.concat(arrayB)
		arrayA.concat(elementA)
*/


console.log(" ");

let taskArrayA = ["drink HTML", "eat JavaScript"];
let taskArrayB = ["inhale CSS", "breath sass"];
let taskArrayC = ["get git", "be node"];

let tasks = taskArrayA.concat(taskArrayB);
console.log("Result from concat method; ");
console.log(tasks);

let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(allTasks);

// This is now combining arrays with elements - similar with push()
let combineTask = taskArrayA.concat("smell express", "throw react");
console.log(combineTask);

/*
	join()
		- returns an array as a string
		- does not change the original array
		- we can use any separator. The default is comma (,)

	Syntax:
		arrayName.join(separatorString)
*/

console.log(" ");

let students = ["Louies", "Aronn", "LA", "Lad"];
console.log(students.join());
console.log(students.join(' '));
console.log(students.join(" - "));


/*
	Iteration Methods
		- are loops designed to perform repetitive tasks in an array. Sued for manipulating array data resulting in complex tasks.
		- normally work with a function supplied as an argument
		- aims to evaluate each element in an array
*/

/*
	forEach()
		- similar to for loop that iterates on each array element

	Synatax:
		arrayname.forEach(function(individualElement) {
		statement
		}) 
*/


console.log(" ");

allTasks.forEach(function(task) {
	console.log(task)
})

// using forEach with conditional statements

let filteredTask = [];

allTasks.forEach(function(task) {
	console.log(task)
	if(task.length > 10) {
		filteredTask.push(task)
	}
})

console.log(allTasks);
console.log("Result of filteredTasks:");
console.log(filteredTask);

/*
	map()
		- iterates on each element and returns a new array with different value depending on the result of the function's operation.

	Syntax:
		arrayName.map(function(individualElement) {
		statement
		})
*/

console.log(" ");

let numbers = [1, 2, 3, 4, 5];
let numberMap = numbers.map(function(number) {
	console.log(number);
	return number*number;
});

console.log("Original Array");
console.log(numbers);
console.log("Result of the MAP method:");
console.log(numberMap);

/*
	every()
		- checks if all elements in an array met the given condition. Returns a true value if all elements meet the condition and false if otherwise.

	Syntax:
		arrayName.every(function(individualElement) {
			return expression/condition
		})
*/

console.log(" ");

let allValid = numbers.every(function(number) {
	return (number < 3);
})

console.log("Result of EVERY method: ")
console.log(allValid);


/*
	some()
		- checks if at least one element in the array meet the given condition. Returns a true value if at least one of the elements meets the given condition and false if otherwise.

	Syntax:
		arrayName.some(function(ind) {
		return expression/condition
		})
*/

let someValid = numbers.some(function(number) {
	return (number <3);
})

console.log("Result of SOME method:")
console.log(someValid);


/*
	filter()
		- returns a new array that contains elements which meets a given condition Return an empty array if no elements were found (that satisfied the given condition)

	Syntax:
		arrayName.filter(function(individualElement) {
			return expression / condition
		})
*/

console.log(" ");

let filteredValid = numbers.filter(function(number) {
	return (number < 5)
})

console.log("Result of filter method:");
console.log(filteredValid);


/*
	includes()
		- checks if the argument passed can be found in an array.
		- can be chained after another method. The result of the first method is used on second method until all chained methods have been resolved.

	Syntax:
		arrayName.includes()

*/

console.log(" ");

let products = ["Mouse", "KEYBOARD", "laptop", "monitor"];

let filteredProducts = products.filter(function(product) {
	return product.toLowerCase().includes("a");
})

console.log("Result of INCLUDE method: ");
console.log(filteredProducts);
